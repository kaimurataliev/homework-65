import React, { Component } from 'react';
import './Pages.css';
import axios from '../../axios';

class Pages extends Component {

    state = {
        info: {},
    };

    componentDidUpdate(prevProps) {
        if(prevProps.match.params.category !== this.props.match.params.category) {
            axios.get(`/${this.props.match.params.category}.json`)
                .then(response => {
                    this.setState({info: response.data});
                })
        }
    }

    componentDidMount () {
        axios.get(`/${this.props.match.params.category}.json`)
            .then(response => {
                this.setState({info: response.data});
            });
    }

    render() {

        return (
            <div className="Page">
                <div className="Content">
                    <h2>{this.state.info.title}</h2>
                    <p>{this.state.info.content}</p>
                </div>
            </div>
        )
    }
}

export default Pages;