import React, { Component } from 'react';
import './Admin.css';
import axios from '../../axios';

export default class Admin extends Component {

    state = {
        title: '',
        content: '',
        category: ''
    };

    editInfo = (event) => {
        event.preventDefault();
        if(this.state.category) {
            axios.put(`/${this.state.category}.json`, {title: this.state.title, content: this.state.content});
        } else {
            alert('Please choose category')
        }
    };

    getTitle = (event) => {
        this.setState({title: event.target.value})
    };

    getContent = (event) => {
        this.setState({content: event.target.value})
    };

    getCategory = (event) => {
        this.setState({category: event.target.value});
        axios.get(`/${event.target.value}.json`)
            .then(response => {
                this.setState(response.data);
            })
    };



    render() {
        return (
            <div className="Admin">
                <form className="form">
                    <h2>Edit pages</h2>
                    <select value={this.state.category} onChange={this.getCategory} name="pages">
                        <option value="home">Home</option>
                        <option value="about">About</option>
                        <option value="portfolio">Portfolio</option>
                        <option value="contacts">Contacts</option>
                    </select>

                    <input value={this.state.title} onChange={this.getTitle} type="text" placeholder="title"/>
                    <textarea value={this.state.content} onChange={this.getContent} cols="40" rows="10"/>
                    <button onClick={this.editInfo}>Save</button>
                </form>
            </div>
        )
    }
}