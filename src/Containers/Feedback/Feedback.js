import React, { Component } from 'react';
import './Feedback.css';
import axios from '../../axios';

export default class Feedback extends Component {

    state = {
        name: '',
        email: '',
        number: ''
    };

    getName = (event) => {
        this.setState({name: event.target.value});
    };

    getEmail = (event) => {
        this.setState({email: event.target.value});
    };

    getNumber = (event) => {
        this.setState({number: event.target.value});
    };

    sendHandler = (e) => {
        e.preventDefault();
        axios.post('/users.json', this.state)
    };

    render () {
        return (
            <div className="Feedback">
                <form className="feedback-form">
                    <h2>Please fill this form and we will contact you</h2>
                    <input onChange={this.getName} value={this.state.name} placeholder="Please enter your name" type="text" />
                    <input onChange={this.getEmail} value={this.state.email} placeholder="please enter your email" type="email" />
                    <input onChange={this.getNumber} value={this.state.number} placeholder="please enter your phone number" type="number" />
                    <button onClick={this.sendHandler}>Send</button>
                </form>
            </div>
        )
    }
}