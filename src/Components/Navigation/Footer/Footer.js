import React from 'react';
import './Footer.css';

const Footer = () => {
    return (
        <div className="footer">
            <p>All rights reserved 2018. HardSkilled Team</p>
        </div>
    )
};

export default Footer;