import React from 'react';
import './Header.css';
import NavigationItems from '../NavigationItems/NavigationItems';

const Header = () => {
    return (
        <header className="Toolbar">
            <nav><NavigationItems /></nav>
        </header>
    )
};

export default Header;