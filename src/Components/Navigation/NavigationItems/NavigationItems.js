import React from 'react';
import './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';

const NavigationItems = () => {
    return (
        <ul className="NavigationItems">
            <NavigationItem to="/pages/home" exact>Home</NavigationItem>
            <NavigationItem to="/pages/about" exact>About</NavigationItem>
            <NavigationItem to="/pages/portfolio" exact>Portfolio</NavigationItem>
            <NavigationItem to="/pages/contacts" exact>Contacts</NavigationItem>
            <NavigationItem to="/pages/feedback" exact>Feedback</NavigationItem>
            <NavigationItem to="/pages/admin">Admin</NavigationItem>
        </ul>
    )
};

export default NavigationItems;