import React, { Fragment } from 'react';
import './Layout.css';
import Header from '../Navigation/Header/Header';
import Footer from '../Navigation/Footer/Footer';

const Layout = props => {
    return (
        <Fragment>
            <Header />
            <main className="Layout-Content">
                {props.children}
            </main>
            <Footer/>
        </Fragment>
    )
};

export default Layout;