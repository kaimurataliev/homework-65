import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import Pages from './Containers/Pages/Pages';
import Feedback from './Containers/Feedback/Feedback';

import Admin from './Containers/Admin/Admin';
import Layout from './Components/Layout/Layout';


class App extends Component {
  render() {
    return (
      <Layout>
          <Switch>
              <Route path="/pages/admin" component={Admin}/>
              <Route path="/pages/feedback" component={Feedback}/>
              <Route path="/pages/:category" component={Pages}/>
              <Route render={() => <h1>not found!</h1>}/>
          </Switch>
      </Layout>
    );
  }
}

export default App;
